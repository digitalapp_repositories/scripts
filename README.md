# PUBLIC DEPLOYMENT AND SERVER STARTUP SCRIPTS #

## Install CentOS ##

1. Launch from bash:

	```
	bash -x setup-centos.sh
	```

1. Launch from repo:

	```
	curl -fsSL https://bitbucket.org/digitalapp_repositories/scripts/downloads/setup-centos.sh | sh
	```


## Laravel Setup ##

1. Params:
	* **project**: Project name

1. Launch from bash:

	```
	bash -x setup-laravel.sh project
	```


1. Launch from repo:

	```
	curl -fsSL https://bitbucket.org/digitalapp_repositories/scripts/downloads/setup-laravel.sh | sh -s -- project
	```


## Spring Setup ##

1. Params:
	* **project**: Project name
	* **profile**: Profile, default "***profile***"
	* **mysqlpassword**: MySQL Root Password
	* **mongopassword**: Optional, if pressent stablish MongoDB Root Password


1. Launch from bash:

	```
	bash -x setup-spring.sh project profile mysqlpassword [mongopassword]
	```


1. Launch from repo:

	```
	curl -fsSL https://bitbucket.org/digitalapp_repositories/scripts/downloads/setup-spring.sh | sh -s -- project profile mysqlpassword [mongopassword]
	```